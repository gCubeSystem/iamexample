# IAM Example

Simple example of IAM interaction in D4Science.

## Authors

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)



## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)


## How to execute this example 

$ python3.6 iamexample.py \<call\> \<client_id\> \<client_secret\> \<context\>

Examples:

$ python3.6 iamexample.py 'AccessToken' 'my_client_id' 'my_client_secret' 

$ python3.6 iamexample.py 'UmaToken' 'my_client_id' 'my_client_secret' '/gcube' 

